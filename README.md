<h1 align="center">
  [Budibase](https://github.com/Budibase/budibase)
</h1>

<h3 align="center">
  Click the above link to navigate to the source repository, and to read the origianl README.md.
</h3>
<p align="center">
  This is a fork for collaborating on the extension of the table component, specifically the implementation of conditional formatting.
</p>
